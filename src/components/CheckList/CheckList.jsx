/* eslint-disable react/prop-types */
import { Typography, TextField, Button } from "@mui/material";
import {
  removeCheckLists,
  getChecklistItems,
  removeCheckItem,
  addCheckItem,
} from "../Services/APICalls";
import { useEffect, useState } from "react";

import GradingIcon from "@mui/icons-material/Grading";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import CheckItems from "../CheckItem/CheckItems";

const CheckList = ({ checklist, updateChecklistState }) => {
  const checklistName = checklist?.name;
  const checklistId = checklist?.id;

  const [checklistItems, setChecklistItems] = useState([]);
  const [checkItemName, setCheckItemName] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState("");
  // const [open, setOpen] = useState(false);

  useEffect(() => {
    setLoading(true);
    getChecklistItems(checklistId)
      .then((res) => {
        setChecklistItems(res.data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleCheckItemDelete = (checkitemID) => {
    removeCheckItem(checklistId, checkitemID)
      .then(() => {
        setChecklistItems({
          checklistItems,
          checkItems: checklistItems?.checkItems?.filter(
            (listItem) => listItem.id !== checkitemID
          ),
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleAddCheckItem = (event) => {
    event.preventDefault();
    if (!checkItemName) {
      return;
    }
    addCheckItem(checklistId, checkItemName).then((res) => {
      setCheckItemName("");
      setChecklistItems({
        checklistItems,
        checkItems: [...checklistItems.checkItems, res.data],
      }).catch((error) => {
        console.log(error);
      });
    });
  };
  if (!checklistItems) {
    return <p>No data</p>;
  }
  if (error) {
    return;
  }

  return (
    <>
      {error ? (
        <p className="error-text">Network Error</p>
      ) : (
        <div key={checklistId}>
          <hr />
          <div className="box">
            <Typography style={{ height: "10px" }}>
              <GradingIcon fontSize="small" />
              {checklistName}
            </Typography>
            <DeleteTwoToneIcon
              fontSize="small"
              onClick={() => {
                removeCheckLists(checklistId, updateChecklistState);
              }}
            />
          </div>

          <div className="add-container">
            {isLoading && <div style={{ fontSize: "2rem" }}>Loading...</div>}
            {error && <p className="error-text">{error}</p>}
            {checklistItems &&
              checklistItems.checkItems?.map((item) => (
                <CheckItems
                  key={item?.id}
                  item={item}
                  checkItemState={item?.state}
                  handleCheckItemDelete={handleCheckItemDelete}
                  cardId={checklistItems.idCard}
                />
              ))}
          </div>

          <form
            onSubmit={(event) => {
              handleAddCheckItem(event);
            }}
          >
            <div className="add-container">
              <TextField
                style={{
                  backgroundColor: "White",
                  width: "40%",
                  marginTop: "10px",
                }}
                value={checkItemName}
                label="Enter check-item name"
                variant="outlined"
                id="outlined"
                placeholder="Enter check-item name"
                size="small"
                onChange={(event) => setCheckItemName(event.target.value)}
              />

              <Button
                type="submit"
                size="medium"
                style={{
                  color: "Black",
                  backgroundColor: "hsl(154, 81%, 58%)",
                  width: "40%",
                }}
              >
                Add check-item
              </Button>
            </div>
          </form>
        </div>
      )}
    </>
  );
};

export default CheckList;
