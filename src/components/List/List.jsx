/* eslint-disable react/prop-types */
import { Card, Typography, Button, TextField } from "@mui/material";
import { removeList, createCard, getAllCards } from "../Services/APICalls";
import { styled } from "@mui/system";
import { useState, useEffect } from "react";

import Cards from "../Cards/Cards";
import RemoveIcon from "@mui/icons-material/Remove";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

// eslint-disable-next-line no-unused-vars
const Container = styled(Card)(({ theme }) => ({
  minWidth: "230px",
  height: "fit-content",
  color: "secondary",
  padding: "16px",
}));

const List = ({ list, updateListsState }) => {
  const [isLoading, setLoading] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [cardName, setCardName] = useState("");
  const [allCards, setAllCards] = useState([]);

  let id = list?.id;
  let title = list?.name;

  useEffect(() => {
    setLoading(true);
    getAllCards(id)
      .then((res) => {
        setAllCards(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  const postCard = (event) => {
    event.preventDefault();
    if (!cardName) {
      return;
    }
    createCard(id, cardName)
      .then((res) => {
        setCardName("");
        setAllCards([...allCards, res.data]);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const updateCardsState = (deletedCardId) => {
    setAllCards(allCards.filter((card) => card.id !== deletedCardId));
  };

  if (!allCards) {
    return <p>No data</p>;
  }

  return (
    <Container key={id}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "5px",
        }}
      >
        <Typography variant="h6">{title}</Typography>
        <DeleteTwoToneIcon onClick={() => removeList(id, updateListsState)} />
      </div>
      <hr />
      {isLoading && (
        <div style={{ color: "olive", fontSize: "1rem" }}>Loading...</div>
      )}

      <br />
      <div>
        {allCards?.map((card) => (
          <Cards
            key={card.id}
            card={card}
            updateCardsState={updateCardsState}
          />
        ))}
      </div>
      {isOpen ? (
        <form
          onSubmit={(event) => {
            postCard(event);
          }}
        >
          <div className="add-container">
            <RemoveIcon onClick={handleClick} />
            <TextField
              style={{
                backgroundColor: "White",
                width: "80%",
                marginTop: "20px",
              }}
              value={cardName}
              label="Enter card name"
              variant="outlined"
              id="outlined"
              placeholder="Enter card name"
              size="small"
              onChange={(event) => setCardName(event.target.value)}
            />
            <Button
              size="medium"
              style={{
                color: "Black",
                backgroundColor: "hsl(149, 59%, 72%)",
              }}
              type="submit"
            >
              Add Card
            </Button>
          </div>
        </form>
      ) : (
        <Typography
          style={{
            width: "6em",
          }}
          fontSize="medium"
          onClick={handleClick}
        >
          + Add List
        </Typography>
      )}
    </Container>
  );
};

export default List;
