/* eslint-disable react/prop-types */
import { useState } from "react";
import {
  TextField,
  Button,
  Card,
  CardContent,
  Icon,
  Snackbar,
} from "@mui/material";
import { createBoard } from "../Services/APICalls";

import cardBackground from "../../assets/add.jpg";

const CreateBoard = ({ updateBoardsState }) => {
  const [newBoardName, setNewBoardName] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const [error, setError] = useState("");
  const [open, setOpen] = useState(false);

  const sendData = (event) => {
    event.preventDefault();
    if (!newBoardName) {
      return;
    }
    createBoard(newBoardName)
      .then((data) => {
        setNewBoardName("");
        updateBoardsState(data);
      })
      .catch((error) => {
        setError(`${error.message}: Adding New Board Failed`);
        setOpen(true);
      });
  };

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        message={error}
      />
      <Card
        style={{
          backgroundImage: `url(${cardBackground})`,
          backgroundSize: "cover",
          width: "230px",
          height: "150px",
          color: "black",
          padding: "16px",
        }}
      >
        {isOpen ? (
          <form
            onSubmit={(event) => {
              sendData(event);
            }}
          >
            <CardContent>
              <TextField
                placeholder="Enter board name"
                style={{
                  backgroundColor: "White",
                  width: "medium",
                  marginTop: "20px",
                }}
                value={newBoardName}
                label="Enter board name"
                variant="outlined"
                onChange={(event) => setNewBoardName(event.target.value)}
              />
              <Button type="submit" size="medium">
                Add Board
              </Button>
            </CardContent>
          </form>
        ) : (
          <Icon
            style={{
              width: "6em",
            }}
            fontSize="medium"
            onClick={handleClick}
          >
            + Add Board
          </Icon>
        )}
      </Card>
    </>
  );
};
export default CreateBoard;
