import { getAllBoardsData } from "../Services/APICalls";
import { useEffect, useState } from "react";
import { Snackbar } from "@mui/material";
import CreateBoard from "./CreateBoard";
import Board from "./Board";

const AllBoards = () => {
  const [boards, setBoards] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [open, setOpen] = useState(true);

  useEffect(() => {
    getAllBoardsData()
      .then((res) => {
        setBoards(res.data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error.message);
        setLoading(false);
      });
  }, []);

  const updateBoardsState = (newBoard) => {
    setBoards([...boards, newBoard]);
  };
  const handleClose = () => {
    setOpen(false);
  };
  if (!boards) {
    return (
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        message="No Boards Found"
      />
    );
  }
  if (error) {
    return <p className="error-text">{error}</p>;
  }

  return (
    <>
      <div className="boards-container">
        {isLoading && <div className="loading-text">Loading...</div>}
        {boards.map((board) => {
          return <Board key={board.id} board={board} />;
        })}
        <CreateBoard updateBoardsState={updateBoardsState} />
      </div>
    </>
  );
};

export default AllBoards;
