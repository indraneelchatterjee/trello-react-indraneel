import { getBoardByID, getListsById, addList } from "../Services/APICalls";
import { TextField, Button, Typography, Card, Snackbar } from "@mui/material";

import { styled } from "@mui/system";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import List from "../List/List";
import RemoveIcon from "@mui/icons-material/Remove";
import cardBackground from "../../assets/addlist.jpg";

// eslint-disable-next-line no-unused-vars
const HeadingText = styled(Typography)(({ theme }) => ({
  backgroundColor: "hsl(148, 24%, 76%)",
  fontSize: "2rem",
  color: "black",
  padding: "10px",
  borderRadius: "10px",
  margin: "10px",
  boxShadow: "0 10px 20px rgba(7,1, 1, 0.1)",
}));

const SingleBoard = () => {
  const { id } = useParams();
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const [singleBoard, setSingleBoard] = useState([]);
  const [lists, setLists] = useState([]);
  const [newListName, setNewListName] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [open, setOpen] = useState(false);

  useEffect(() => {
    getDataById();
    getLists();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getDataById = () => {
    getBoardByID(id)
      .then((res) => {
        setSingleBoard(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("hi");
        setError(`${error.message} : Fetching Board Failed`);
        setLoading(false);
      });
  };

  const getLists = () => {
    getListsById(id)
      .then((res) => {
        setLists(res.data);
        setLoading(false);
      })
      .catch((error) => {
        setError(`${error.message} : Fetching Lists Failed`);
        setLoading(false);
      });
  };

  const addNewList = (event) => {
    event.preventDefault();
    addList(newListName, id)
      .then((res) => {
        setNewListName("");
        setLists([...lists, res.data]);
      })
      .catch((error) => {
        setErrorMessage(`${error.message} : Adding New List Failed`);
        setOpen(true);
      });
  };
  const updateListsState = (deletedList) => {
    setLists(lists.filter((list) => list.id !== deletedList.id));
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    setIsOpen(!isOpen);
  };
  if (!lists) {
    return (
      <Snackbar
        open={true}
        autoHideDuration={5000}
        onClose={handleClose}
        message="No Lists Found"
      />
    );
  }

  if (error) {
    return <p className="error-text">{error}</p>;
  }
  return (
    <>
      <div>
        <Snackbar
          open={open}
          autoHideDuration={5000}
          onClose={handleClose}
          message={errorMessage}
        />
        <HeadingText>{singleBoard?.name}</HeadingText>
        <div className="lists-container">
          {lists.map((list) => (
            <List
              key={list.id}
              updateListsState={updateListsState}
              list={list}
            ></List>
          ))}
          {isLoading && <div className="loading-text">Loading...</div>}
          <Card
            style={{
              backgroundImage: `url(${cardBackground})`,
              backgroundSize: "cover",
              minWidth: "230px",
              height: "150px",
              color: "black",
              padding: "16px",
            }}
          >
            {isOpen ? (
              <div className="add-container">
                <RemoveIcon onClick={handleClick} />
                <form
                  onSubmit={(event) => {
                    addNewList(event);
                  }}
                >
                  <TextField
                    style={{
                      backgroundColor: "White",
                      width: "80%",
                    }}
                    value={newListName}
                    label="Enter card name"
                    variant="outlined"
                    id="outlined"
                    placeholder="Enter list name"
                    size="small"
                    onChange={(event) => setNewListName(event.target.value)}
                  />
                  <Button
                    type="submit"
                    size="medium"
                    style={{
                      color: "Black",
                      backgroundColor: "hsl(149, 59%, 72%)",
                    }}
                  >
                    Add List
                  </Button>
                </form>
              </div>
            ) : (
              <Typography
                style={{
                  width: "6em",
                }}
                fontSize="medium"
                onClick={handleClick}
              >
                + Add List
              </Typography>
            )}
          </Card>
        </div>
      </div>
    </>
  );
};

export default SingleBoard;
