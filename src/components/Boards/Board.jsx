/* eslint-disable react/prop-types */
import { Card, CardContent, Typography } from "@mui/material";
import { Link } from "react-router-dom";

const Board = ({ board }) => {
  let id = board?.id;
  let url = board?.prefs.backgroundImage;
  let name = board?.name;
  return (
    <Link to={`./${id}`} className="link">
      <Card
        key={id}
        style={{
          backgroundImage: `url(${url})`,
          backgroundSize: "cover",
          width: "230px",
          height: "150px",
          color: "secondary",
          padding: "16px",
        }}
      >
        <CardContent>
          <Typography variant="h6">{name}</Typography>
        </CardContent>
      </Card>
    </Link>
  );
};

export default Board;
