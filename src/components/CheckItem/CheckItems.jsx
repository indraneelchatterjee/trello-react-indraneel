/* eslint-disable react/prop-types */
import {
  Card,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Snackbar,
} from "@mui/material";
import { useState } from "react";
import { updateCheckItem } from "../Services/APICalls";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

const CheckItems = ({
  item,
  handleCheckItemDelete,
  checkItemState,
  cardId,
}) => {
  const [isChecked, setChecked] = useState(checkItemState === "complete");
  const [error, setError] = useState("");
  const [open, setOpen] = useState(false);

  let checkItemId = item?.id;
  let checkItemName = item?.name;

  const handleCheckBox = () => {
    let state = !isChecked ? "complete" : "incomplete";
    updateCheckItem(cardId, checkItemId, state)
      .then(() => {
        setChecked(!isChecked);
      })
      .catch((error) => {
        setError(`${error.message}: Updating checkbox failed`);
        setOpen(true);
      });
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        message={error}
      />
      <Card className="card-container">
        <FormGroup>
          <FormControlLabel
            control={<Checkbox checked={isChecked} onClick={handleCheckBox} />}
            label={checkItemName}
            style={{ height: "10px" }}
          />
        </FormGroup>

        <DeleteTwoToneIcon
          fontSize="small"
          style={{ color: "red" }}
          onClick={() => {
            handleCheckItemDelete(checkItemId);
          }}
        />
      </Card>
    </>
  );
};

export default CheckItems;
