import { TOKEN, KEY } from "../../../config";

import axios from "axios";

export const getAllBoardsData = () => {
  return axios.get(
    `https://api.trello.com/1/members/me/boards?key=${KEY}&token=${TOKEN}`
  );
};

export const getBoardByID = (id) => {
  return axios.get(
    `https://api.trello.com/1/boards/${id}?key=${KEY}&token=${TOKEN}`
  );
};

export const getListsById = (id) => {
  return axios.get(
    `https://api.trello.com/1/boards/${id}/lists?key=${KEY}&token=${TOKEN}`
  );
};

export const addList = (inputName, id) => {
  return axios.post(
    ` https://api.trello.com/1/lists?name=${inputName}&idBoard=${id}&key=${KEY}&token=${TOKEN}`
  );
};

export const removeList = async (id, updateListsState) => {
  await axios
    .put(
      `https://api.trello.com/1/lists/${id}/closed?value=true&key=${KEY}&token=${TOKEN}`
    )
    .then((res) => {
      updateListsState(res.data);
    })
    .catch((error) => {
      console.log(error);
    });
};

export const createBoard = async (newBoardName) => {
  const response = await axios.post(
    `https://api.trello.com/1/boards/?name=${newBoardName}&key=${KEY}&token=${TOKEN}`
  );
  const data = response.data;
  return data;
};

export const createCard = (listId, cardName) => {
  return axios.post(
    `https://api.trello.com/1/cards?idList=${listId}&key=${KEY}&token=${TOKEN}`,
    { name: `${cardName}` }
  );
};

export const getAllCards = async (listId) => {
  return axios.get(
    `https://api.trello.com/1/lists/${listId}/cards?key=${KEY}&token=${TOKEN}`
  );
};

export const removeCard = (cardId, updateCardsState) => {
  axios
    .delete(
      `https://api.trello.com/1/cards/${cardId}?key=${KEY}&token=${TOKEN}`
    )
    .then(() => {
      updateCardsState(cardId);
    })
    .catch((error) => {
      console.log(error);
    });
};

export const getCheckLists = (cardId) => {
  return axios.get(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${KEY}&token=${TOKEN}`
  );
};

export const removeCheckLists = (checklistId, updateChecklistState) => {
  axios
    .delete(
      `https://api.trello.com/1/checklists/${checklistId}?key=${KEY}&token=${TOKEN}`
    )
    .then(() => {
      updateChecklistState(checklistId);
    })
    .catch((error) => {
      console.log(error);
    });
};

export const addCheckList = (cardId, checklistName) => {
  return axios.post(
    `https://api.trello.com/1/checklists?idCard=${cardId}&key=${KEY}&token=${TOKEN}`,
    { name: `${checklistName}` }
  );
};

export const getChecklistItems = (checklistId) => {
  return axios.get(
    `https://api.trello.com/1/checklists/${checklistId}?key=${KEY}&token=${TOKEN}`
  );
};

export const removeCheckItem = (checklistId, checkItemId) => {
  return axios.delete(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${KEY}&token=${TOKEN}`
  );
};

export const addCheckItem = (checklistId, checkItemName) => {
  return axios.post(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkItemName}&key=${KEY}&token=${TOKEN}`
  );
};

export const updateCheckItem = (cardId, checkItemId, state) => {
  return axios.put(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${state}&key=${KEY}&token=${TOKEN}`
  );
};
