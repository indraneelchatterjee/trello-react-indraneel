/* eslint-disable react/prop-types */
import { useState, useEffect } from "react";
import { Card, Modal, Button, TextField, Snackbar } from "@mui/material";
import { removeCard, getCheckLists, addCheckList } from "../Services/APICalls";

import CheckList from "../CheckList/CheckList";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

let Cards = ({ card, updateCardsState }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [allChecklists, setChecklists] = useState([]);
  const [checklistName, setChecklistName] = useState("");
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [open, setOpen] = useState(false);
  let id = card?.id;
  let name = card?.name;

  useEffect(() => {
    getCheckLists(id)
      .then((res) => {
        setChecklists(res.data);
        setLoading(false);
      })
      .catch((error) => {
        setError(`${error.message} : Fetching Checklists failed`);
        setLoading(false);
      });
  }, []);
  const handleModal = () => {
    setIsOpen(!isOpen);
  };

  const updateChecklistState = (deletedChecklistId) => {
    setChecklists(
      allChecklists.filter((checklist) => checklist.id !== deletedChecklistId)
    );
  };
  const handleChecklistAdd = () => {
    event.preventDefault();
    if (!checklistName) {
      return;
    }
    addCheckList(id, checklistName)
      .then((res) => {
        setChecklistName("");
        setChecklists([...allChecklists, res.data]);
      })
      .catch((error) => {
        setErrorMessage(`${error.message}: Adding New Checklist Failed.`);
        setOpen(true);
      });
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (!allChecklists) {
    return (
      <Snackbar
        open={true}
        autoHideDuration={5000}
        onClose={handleClose}
        message="No Checklists Found"
      />
    );
  }
  if (error) {
    <p className="error-text">{error}</p>;
  }
  return (
    <>
      <div key={id}>
        <Card
          className="card-container"
          onClick={() => {
            setIsOpen(true);
          }}
        >
          {name}
          <DeleteTwoToneIcon
            style={{ color: "red" }}
            onClick={(event) => {
              event.stopPropagation();
              removeCard(id, updateCardsState);
            }}
          />
        </Card>
        <div>
          <Modal
            className="modal-container"
            open={isOpen}
            onClose={handleModal}
          >
            <div>
              <div>
                <Snackbar
                  open={open}
                  autoHideDuration={5000}
                  onClose={handleClose}
                  message={errorMessage}
                />
                {isLoading && <div className="loading-text">Loading...</div>}
                {allChecklists.map((checklist) => (
                  <CheckList
                    key={checklist.id}
                    checklist={checklist}
                    updateChecklistState={updateChecklistState}
                  />
                ))}
              </div>
              <form
                onSubmit={(event) => {
                  handleChecklistAdd(event);
                }}
              >
                <div className="add-container" style={{ width: "40vw" }}>
                  <hr />
                  <TextField
                    style={{
                      backgroundColor: "White",
                      width: "50%",
                      marginTop: "20px",
                    }}
                    value={checklistName}
                    label="Enter checklist name"
                    variant="outlined"
                    id="outlined"
                    placeholder="Enter checklist name"
                    size="small"
                    onChange={(event) => setChecklistName(event.target.value)}
                  />
                  <Button
                    size="medium"
                    style={{
                      color: "Black",
                      backgroundColor: "hsl(149, 59%, 72%)",
                      width: "50%",
                    }}
                    type="submit"
                  >
                    Add Checklist
                  </Button>
                </div>
              </form>
            </div>
          </Modal>
        </div>
      </div>
    </>
  );
};

export default Cards;
