import { AppBar, Toolbar, TextField } from "@mui/material";
import { Link } from "react-router-dom";

import trelloIcon from "../../assets/trello-official.svg";

function NavBar() {
  return (
    <>
      <AppBar
        position="static"
        style={{ backgroundColor: "hsl(144, 17%, 55%)" }}
      >
        <Toolbar>
          <Link to="./boards">
            <img src={trelloIcon} alt="Logo" style={{ height: 35 }} />
          </Link>

          <div>
            <TextField id="outlined-basic" label="Search" variant="outlined" />
          </div>
        </Toolbar>
      </AppBar>
    </>
  );
}

export default NavBar;
