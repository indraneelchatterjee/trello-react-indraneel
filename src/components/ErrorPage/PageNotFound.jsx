const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    textAlign: "center",
    margin: "5%",
    backgroundColor: "white",
    width: "90%",
    height: "400px",
  },
  heading: {
    fontSize: "24px",
    fontWeight: "bold",
    marginBottom: "10px",
    color: "red",
  },
  text: {
    fontSize: "16px",
  },
};

const PageNotFound = () => {
  return (
    <div style={styles.container}>
      <h1 style={styles.heading}>404 - Page Not Found</h1>
      <p style={styles.text}>
        Sorry, the page you are looking for does not exist.
      </p>
      <p style={styles.text}>Click on the Trello Logo to redirect..</p>
    </div>
  );
};

export default PageNotFound;
