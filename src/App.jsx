import { Route, Routes, Navigate } from "react-router-dom";

import NavBar from "./components/NavBar/NavBar";
import HomePage from "./components/HomePage/HomePage";
import SingleBoard from "./components/Boards/SingleBoard";
import PageNotFound from "./components/ErrorPage/PageNotFound";

function App() {
  return (
    <>
      <NavBar></NavBar>
      <Routes>
        <Route exact path="/" element={<Navigate to="/boards" replace />} />
        <Route path="/boards" element={<HomePage />}></Route>

        <Route path="/boards/:id" element={<SingleBoard />}></Route>
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </>
  );
}

export default App;
